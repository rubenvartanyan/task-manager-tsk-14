package ru.vartanyan.tm.controller;

import ru.vartanyan.tm.api.controller.ICommandController;
import ru.vartanyan.tm.api.service.ICommandService;
import ru.vartanyan.tm.model.Command;
import ru.vartanyan.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ruben Vartanyan");
        System.out.println("E-MAIL: rvartanyan@tsconsulting.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showInfo() {
        System.out.println("[SYSTEM INFO]");

        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory (bytes): " + NumberUtil.format(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final long maxMemoryValue = isMaxMemory ? Long.parseLong("no limit") : maxMemory;
        System.out.println("Maximum memory (bytes): " + NumberUtil.format(maxMemoryValue));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM (bytes): " + NumberUtil.format(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));

        System.out.println("[OK]");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command.getName() == null) continue;
            System.out.println(command.getName());
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command.getArg() == null) continue;
            System.out.println(command.getArg());
        }
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
