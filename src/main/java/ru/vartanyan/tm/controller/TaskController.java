package ru.vartanyan.tm.controller;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.controller.ITaskController;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(ITaskService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> list;
        if (sort == null || sort.isEmpty()) list = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            list = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task: list) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(Task task) {
        if (task == null) return;
        System.out.println("[ID] " + task.getId());
        System.out.println("[NAME] " + task.getName());
        System.out.println("[DESCRIPTION] " + task.getDescription());
    }


    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        else System.out.println("[TASK REMOVED]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        else taskService.removeOneById(id);
        System.out.println("[TASK REMOVED]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        else System.out.println("[TASK REMOVED]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK UPDATED]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER Id]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK UPDATED]");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK CREATED]");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK STARTED]");
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK STARTED]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK STARTED]");
    }

    @Override
    public void finishTaskById() {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK FINISHED]");
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK FINISHED]");
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK FINISHED]");
    }

    @Override
    public void updateTaskStatusById() {
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.updateTaskStatusById(id, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK STATUS UPDATED]");
    }

    @Override
    public void updateTaskStatusByName() {
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.updateTaskStatusByName(name, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK STATUS UPDATED]");
    }

    @Override
    public void updateTaskStatusByIndex() {
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.updateTaskStatusByIndex(index, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK STATUS UPDATED]");
    }

    @Override
    public void findAllByProjectId() {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTaskByProjectId(projectId);
        int index = 1;
        for (Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void bindTaskByProjectId() {
        System.out.println("BIND TASK TO PROJECT BY PROJECT ID");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskByProjectId(projectId, taskId);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("TASK ADDED TO PROJECT");
    }

    @Override
    public void unbindTaskByProjectId() {
        System.out.println("UNBIND TASK TO PROJECT BY PROJECT ID");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("TASK REMOVED FROM PROJECT");
    }

}
