package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // SHOW ALL TASKS FROM PROJECT
    List<Task> findAllTaskByProjectId(String projectId);

    // ADD TASK TO PROJECT
    Task bindTaskByProjectId(String projectId, String taskId);

    // REMOVE TASK FROM PROJECT
    Task unbindTaskFromProject(String projectId, String taskId);

    // REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT
    Project removeProjectById(String projectId);

}
