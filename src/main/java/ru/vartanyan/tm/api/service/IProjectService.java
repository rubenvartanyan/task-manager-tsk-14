package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project add(Project project);

    Project remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    void clear();

    Project add(String name, String description);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project startProjectByIndex(Integer index);

    Project finishProjectById(String id);

    Project finishProjectByName(String name);

    Project finishProjectByIndex(Integer index);

    Project updateProjectStatusById(String id, Status status);

    Project updateProjectStatusByName(String name, Status status);

    Project updateProjectStatusByIndex(Integer index, Status status);

}
