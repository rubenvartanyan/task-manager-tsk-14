package ru.vartanyan.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showInfo();

    void showHelp();

    void showCommands();

    void showArguments();

    void exit();

}
