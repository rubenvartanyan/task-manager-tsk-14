package ru.vartanyan.tm.api.controller;

public interface IProjectController {

    void showList();

    void showProjectByIndex();

    void showProjectByName();

    void showProjectById();

    void removeProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

    void create();

    void clear();

    void startProjectById();

    void startProjectByName();

    void startProjectByIndex();

    void finishProjectById();

    void finishProjectByName();

    void finishProjectByIndex();

    void updateProjectStatusById();

    void updateProjectStatusByName();

    void updateProjectStatusByIndex();

    void removeProjectAndTaskById();

}
