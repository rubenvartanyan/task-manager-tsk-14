package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(list);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> finaAllByProjectId(String projectId) {
        final List<Task> listOfTask = new ArrayList<>();
        for (Task task: list){
            if (task.getProjectId().equals(projectId)) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public List<Task> removeAllByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        for (Task task: list) {
            if (task.getProjectId().equals(projectId)) task.setProjectId("");
        }
        return list;
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(String projectId, String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId("");
        return task;
    }

    @Override
    public Task findOneById(final String id) {
        for (Task task: list) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (Task task: list) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        list.add(task);
        return task;
    }

    @Override
    public Task remove(final Task task) {
        list.remove(task);
        return task;
    }

    @Override
    public void clear() {
        list.clear();
    }

}
