package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findOneById(final String id) {
        for (Project project: list) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (Project project: list) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project add(final Project project) {
        list.add(project);
        return project;
    }

    @Override
    public Project remove(final Project project) {
        list.remove(project);
        return project;
    }

    @Override
    public void clear() {
        list.clear();
    }

}
